//I used 4 simple TypeScript methods to output the "Hello world" phrase. These 4 methods are the 

//1. Basic
let msg: string = 'Hello world'

console.log(msg)

//2. addition
let msg2: string[] = ['Hello', 'world']

console.log(msg2[0] + ' '+ msg2[1])


//3. basic for loop
for (let word of msg2) {
    console.log(word)
}

//4. function
function printStuff(a, b) {
    console.log(a + ' ' + b)
}

printStuff('Hello', 'world')